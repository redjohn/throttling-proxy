require './proxy_services_pb'

def main
  stub = Epic::Lulz::ThrottlingProxy::Stub.new('localhost:9002', :this_channel_is_insecure)
  urls = stub.add_urls(Epic::Lulz::ThrottledUrls.new(urls: {"http://www.google.com/" => 1500})).urls
  p "URLS: #{urls.to_h.inspect}"
end

main
