package epic.lulz

import java.nio.ByteBuffer
import java.util.{ List => JList }

import scala.concurrent.{ ExecutionContext, Future }
import scala.concurrent.duration.{ Duration, DurationLong }

import org.eclipse.jetty.client.api.Response
import org.eclipse.jetty.proxy.AsyncMiddleManServlet
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.servlet.{ ServletHandler, ServletHolder }

import epic.lulz.proxy.{ ResetUrls, ThrottledUrls, ThrottlingProxyGrpc, UrlsReset }
import io.circe._
import io.circe.parser._
import io.grpc.ServerBuilder
import javax.servlet.http.{ HttpServletRequest, HttpServletResponse }

case class ProxyConfig(port: Int = 9001, interfacePort: Int = 9002, urls: Map[String, Int] = Map.empty)

object Proxy extends App {
  val parser = new scopt.OptionParser[ProxyConfig]("proxy") {
    opt[Int]('p', "port")
      .action { (port, cfg) => cfg.copy(port = port) }
      .text("The port the proxy will lsten on")

    opt[Int]('i', "interface")
      .action { (port, cfg) => cfg.copy(interfacePort = port) }
      .text("The port the RPC interface will lsten on")

    opt[String]('u', "urls")
      .action { (urls, cfg) =>
        decode[Map[String, Int]](urls) match {
          case Right(urlsToThrottle) => cfg.copy(urls = urlsToThrottle)
          case _ => {
            println("Unable to parse provided URLs")
            System.exit(1)
            cfg
          }
        }
      }
      .text("A JSON object mapping URLs to be throttled to the desired speed")
  }

  parser.parse(args, ProxyConfig()) match {
    case Some(cfg) => {
      val servlet = startRpcInterface(cfg)
      startProxy(cfg, servlet)
    }
    case None => {
      println("Could not parse arguments")
      System.exit(1)
    }
  }

  def startRpcInterface(cfg: ProxyConfig): ThrottlingMiddleManServlet = {
    val servlet = new ThrottlingMiddleManServlet(cfg.urls)
    val server = ServerBuilder
      .forPort(cfg.interfacePort)
      .addService(ThrottlingProxyGrpc.bindService(new ProxyInterface(servlet), ExecutionContext.global))
      .build
      .start
    println("RPC interface started, listening on " + cfg.interfacePort)
    sys.addShutdownHook { server.shutdown() }
    servlet
  }

  def startProxy(cfg: ProxyConfig, servlet: ThrottlingMiddleManServlet): Unit  = {
    val server = new Server(cfg.port)
    val holder = new ServletHolder(servlet)
    holder.setInitParameter("maxThreads", "4")
    val handler = new ServletHandler()
    handler.addServletWithMapping(holder, "/*")
    server.setHandler(handler)
    server.start()
    server.join()
  }
}

class ThrottlingMiddleManServlet(urlsToThrottle: Map[String, Int]) extends AsyncMiddleManServlet {
  private var urls = urlsToThrottle

  override def newServerResponseContentTransformer(
    request: HttpServletRequest,
    proxyResponse: HttpServletResponse,
    serverResponse: Response
  ) = {
    println(urls)
    val requestUrl = request.getRequestURL.toString
    println(requestUrl)
    if (urls.contains(requestUrl)) {
      println(s"THROTTLING TO ${urls(requestUrl)}")
      new RequestThrottlingTransformer(urls(requestUrl))
    } else {
      AsyncMiddleManServlet.ContentTransformer.IDENTITY
    }
  }

  def addUrls(urlsToThrottle: Map[String, Int]): Map[String, Int] = {
    urls ++= urlsToThrottle
    urls
  }

  def reset(): Unit = {
    urls = Map.empty
  }
}

case class RequestThrottlingTransformer(speed: Int) extends AsyncMiddleManServlet.ContentTransformer {
  override def transform(input: ByteBuffer, finished: Boolean, output: JList[ByteBuffer]): Unit = {
    Thread.sleep(RequestThrottler.delayForBytes(bytes = input.remaining, speed = speed).toMillis)
    output.add(input)
  }
}

object RequestThrottler {
  def delayForBytes(bytes: Long, speed: Int): Duration = (bytes / speed).seconds
}

class ProxyInterface(servlet: ThrottlingMiddleManServlet) extends ThrottlingProxyGrpc.ThrottlingProxy {
  override def addUrls(urls: ThrottledUrls) = {
    Future.successful(ThrottledUrls(servlet.addUrls(urls.urls)))
  }

  override def reset(req: ResetUrls) = {
    servlet.reset()
    Future.successful(UrlsReset())
  }
}