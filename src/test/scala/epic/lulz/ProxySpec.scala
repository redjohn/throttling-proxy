package epic.lulz

import java.nio.ByteBuffer
import java.util.ArrayList

import scala.concurrent.duration.DurationInt

import org.eclipse.jetty.client.api.Response
import org.eclipse.jetty.proxy.AsyncMiddleManServlet
import org.scalamock.scalatest.MockFactory
import org.scalatest.{ FunSpec, Matchers }

import javax.servlet.http.{ HttpServletRequest, HttpServletResponse }

class ProxySpec extends FunSpec with Matchers with MockFactory {

  describe("RequestThrottler") {
    it("calculates the delay for a given speed") {
      val duration = RequestThrottler.delayForBytes(bytes = 2000, speed = 1000)

      duration should equal(2.seconds)
    }
  }

  describe("RequestThrottlingTransformer") {
    case class Fixture(input: ByteBuffer, transformer: RequestThrottlingTransformer)

    object Fixture{
      def apply(bytes: Int, speed: Int) = {
        val data = Stream.fill(bytes)("x").mkString.getBytes
        val input = ByteBuffer.wrap(data)
        val transformer = new RequestThrottlingTransformer(speed)
        new Fixture(input, transformer)
      }
    }

    it("delays the response") {
      val Fixture(input, transformer) = Fixture(bytes = 2000, speed = 1000)
      val before = System.currentTimeMillis()
      transformer.transform(input, true, new ArrayList[ByteBuffer]())
      val after = System.currentTimeMillis()

      (after - before) should equal(2.seconds.toMillis +- 50)
    }

    it("returns the response data unchanged") {
      val Fixture(input, transformer) = Fixture(bytes = 2000, speed = 1000)
      val output = new ArrayList[ByteBuffer]()
      transformer.transform(input, true, output)

      output.size should equal(1)
      output.get(0) should equal(input)
    }
  }

  describe("ThrottlingMiddleManServlet") {
    case class Fixture(
      servlet: ThrottlingMiddleManServlet,
      request: HttpServletRequest,
      proxyResponse: HttpServletResponse,
      serverResponse: Response
    )

    object Fixture {
      def apply(urls: Option[Map[String, Int]] = None) = {
        new Fixture(
          servlet = new ThrottlingMiddleManServlet(urls.getOrElse(Map("http://throttle-me.com" -> 1000))),
          request = stub[HttpServletRequest],
          proxyResponse = mock[HttpServletResponse],
          serverResponse = mock[Response]
        )
      }
    }

    it("uses a RequestThrottlingTransformer if the url should be throttled") {
      val Fixture(servlet, request, proxyResponse, serverResponse) = Fixture()
      (request.getRequestURL _).when().returns(new StringBuffer("http://throttle-me.com"))
      val transformer = servlet.newServerResponseContentTransformer(request, proxyResponse, serverResponse)

      transformer should equal(new RequestThrottlingTransformer(1000))
    }

    it("does not throttle urls unless configured to") {
      val Fixture(servlet, request, proxyResponse, serverResponse) = Fixture()
      (request.getRequestURL _).when().returns(new StringBuffer("http://dont-throttle-me-bro.com"))
      val transformer = servlet.newServerResponseContentTransformer(request, proxyResponse, serverResponse)

      transformer should equal(AsyncMiddleManServlet.ContentTransformer.IDENTITY)
    }

    it("allows throttled URLs to be added") {
      val Fixture(servlet, request, proxyResponse, serverResponse) = Fixture()
      servlet.addUrls(Map("http://throttle-this.com" -> 2000))
      (request.getRequestURL _).when().returns(new StringBuffer("http://throttle-this.com"))
      val transformer = servlet.newServerResponseContentTransformer(request, proxyResponse, serverResponse)

      transformer should equal(new RequestThrottlingTransformer(2000))
    }

    it("doesn't remove throttled URLs when new ones are added") {
      val Fixture(servlet, request, proxyResponse, serverResponse) = Fixture()
      servlet.addUrls(Map("http://throttle-this.com" -> 2000))
      (request.getRequestURL _).when().returns(new StringBuffer("http://throttle-me.com"))
      val transformer = servlet.newServerResponseContentTransformer(request, proxyResponse, serverResponse)

      transformer should equal(new RequestThrottlingTransformer(1000))
    }

    it("allows throttled URLs to be reset") {
      val Fixture(servlet, request, proxyResponse, serverResponse) = Fixture()
      servlet.reset()
      (request.getRequestURL _).when().returns(new StringBuffer("http://throttle-me.com"))
      val transformer = servlet.newServerResponseContentTransformer(request, proxyResponse, serverResponse)

      transformer should equal(AsyncMiddleManServlet.ContentTransformer.IDENTITY)
    }

    it("returns the full list of throttled urls when urls are added") {
      val Fixture(servlet, _, _, _) = Fixture()
      val urls = servlet.addUrls(Map("http://throttle-this.com" -> 2000))
      urls should equal(Map("http://throttle-this.com" -> 2000, "http://throttle-me.com" -> 1000))
    }
  }

}