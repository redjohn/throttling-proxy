lazy val proxy = (project in file("."))
  .enablePlugins(JavaAppPackaging)
  .settings(
    version := "0.1.0",
    scalaVersion := "2.12.10",
    PB.targets in Compile := Seq(
      scalapb.gen() -> (sourceManaged in Compile).value
    ),
    libraryDependencies ++= Seq(
      "org.eclipse.jetty" % "jetty-server" % "9.4.7.v20170914",
      "org.eclipse.jetty" % "jetty-servlet" % "9.4.7.v20170914",
      "org.eclipse.jetty" % "jetty-proxy" % "9.4.7.v20170914",
      "com.twitter" %% "util-core" % "17.11.0",
      "io.circe" %% "circe-core" % "0.8.0",
      "io.circe" %% "circe-generic" % "0.8.0",
      "io.circe" %% "circe-parser" % "0.8.0",
      "com.github.scopt" %% "scopt" % "3.7.0",
      "io.grpc" % "grpc-netty" % com.trueaccord.scalapb.compiler.Version.grpcJavaVersion,
      "com.trueaccord.scalapb" %% "scalapb-runtime-grpc" % com.trueaccord.scalapb.compiler.Version.scalapbVersion,
      "org.scalatest" %% "scalatest" % "3.0.4" % Test,
      "org.scalamock" %% "scalamock" % "4.0.0" % Test
    ),
    packageName in Docker := "epiclulz/throttling-proxy",
    dockerExposedPorts := Seq(9001, 9002)
  )
